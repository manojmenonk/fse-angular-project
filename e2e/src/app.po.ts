import { browser, by, element } from 'protractor';

export class AppPage {
  navigateToTask() {
    return browser.get('/task/add');
  }

  geTtaskHeaderText() {
    return element(by.css('header h4')).getText() ;
  }
}
