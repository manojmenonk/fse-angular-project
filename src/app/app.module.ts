//modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule,ToastrService } from 'ngx-toastr';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import {NgbModule, NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
//componenets
import { AppComponent } from './app.component';
import { CreateComponent as TaskCreateComponent } from './modules/task-manager/components/create/create.component';
import { ViewComponent as TaskViewComponent} from './modules/task-manager/components/view/view.component';
import { ParentSearchComponent } from './modules/task-manager/components/search/parent-search/parent-search.component';

//services 
import { ParentTaskService } from './modules/task-manager/services/parent-task.service';
import { AlertService } from './modules/shared/services/alert.service';
import {SlimLoadingBarService } from 'ng2-slim-loading-bar';

//routing
import { RouterModule, Routes } from '@angular/router';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';

//directive
import { DateCompareValidatorDirective } from './modules/shared/directives/datecompare.directive';
import { NgbDateMomentParserFormatter } from './modules/shared/date-formatter';

//routes
const routes: Routes = [
   { path: '', redirectTo: 'task/add', pathMatch: 'full' },
   {
    path: 'task',
    children: [
            { path: 'add', component: TaskCreateComponent },
            { path: 'view', component: TaskViewComponent }
    ]
  }
];
@NgModule({
  declarations: [
    AppComponent,
    TaskCreateComponent,
    TaskViewComponent,
    ParentSearchComponent,
    DateCompareValidatorDirective
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    SlimLoadingBarModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    AngularFontAwesomeModule,
    NgbModule
  ],
  providers: [
    ParentTaskService,
    AlertService,
    ToastrService,
    SlimLoadingBarService,
   // {provide: NgbDateParserFormatter, useFactory: () => { return new NgbDateMomentParserFormatter("DD/MM/YYYY") } }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
