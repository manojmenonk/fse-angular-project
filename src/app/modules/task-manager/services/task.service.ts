import { Injectable } from '@angular/core';
import { HttpParams, HttpClient } from '@angular/common/http';

import { environment } from '../../../../environments/environment';
import { Task } from '../models/task';
import { ApiResponse } from '../../shared/models/shared';

import { Observable } from 'rxjs';
//import 'rxjs/add/operator/map';
//import 'rxjs/add/operator/catch';
//import 'rxjs/add/observable/throw';

export interface ITaskService {
  getTask(taskId: number): Observable<ApiResponse<Task>>;
  getTasksList(sortKey?:string): Observable<ApiResponse<Task[]>>;
  getByTasksName(searchKey?:string): Observable<ApiResponse<Task[]>>;
  searchByParentTask(searchKey?:string, criteria?:string): Observable<ApiResponse<Task[]>>;
  addTask(newUser: Task): Observable<ApiResponse<Task>>;
  editTask(updateUser: Task): Observable<ApiResponse<Task>>;
}

@Injectable({
  providedIn: 'root'
})
export class TaskService implements ITaskService {

  baseUri = environment.apiBaseUri;

  constructor(private http: HttpClient) { }

  
  getTask(taskId: number): Observable<ApiResponse<Task>> {

    var uri= `${this.baseUri}${environment.endpoint_task_get}/${taskId}`;

    return this.http
      .get<ApiResponse<Task>>(uri);
  }

  getTasksList(sortKey?:string): Observable<ApiResponse<Task[]>> {
    //add query string params to search and sort
    
    let params = new HttpParams();
    if (sortKey)
      params = params.append('sortKey', sortKey);

    var uri = `${this.baseUri}${environment.endpoint_task_get}`;
    console.log("uri: "+uri);
    return this.http
      .get<ApiResponse<Task[]>>(uri, { params: params });
  }

  getByTasksName(searchKey?:string, criteria?:string): Observable<ApiResponse<Task[]>> {
    //add query string params to search and sort
    
    let params = new HttpParams();
    if (searchKey)
      params = params.append('searchKey', searchKey);
      params = params.append('criteria', criteria);
    var uri = `${this.baseUri}${environment.endpoint_task_get}`;

    return this.http
      .get<ApiResponse<Task[]>>(uri, { params: params });
  }

  searchByParentTask(searchKey?:string, criteria?:string): Observable<ApiResponse<Task[]>> {
    let params = new HttpParams();
    if (searchKey)
      params = params.append('searchKey', searchKey);
      params = params.append('criteria', criteria);
    var uri = `${this.baseUri}${environment.endpoint_parent_search}`;

    return this.http
      .get<ApiResponse<Task[]>>(uri, { params: params });
  }

  addTask(newTask: Task): Observable<ApiResponse<Task>> {

    var uri = `${this.baseUri}${environment.endpoint_task_add}`;

    return this.http
      .post<ApiResponse<Task>>(uri, newTask);
  }

  editTask(updateTask: Task): Observable<ApiResponse<Task>>
  {
    var uri = `${this.baseUri}${environment.endpoint_task_edit}`;

    return this.http
      .post<ApiResponse<Task>>(uri, updateTask);
  }

  endTask(taskId: number): Observable<ApiResponse<Task>>
  {
    var uri = `${this.baseUri}${environment.endpoint_task_delete}/${taskId}`;

    return this.http
      .get<ApiResponse<Task>>(uri);
  }
}
