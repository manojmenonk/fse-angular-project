import { TestBed } from '@angular/core/testing';
import { environment } from '../../../../environments/environment';
import { TaskService } from './task.service';
import { HttpParams, HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { Task } from '../models/task';
import { ApiResponse } from '../../shared/models/shared';
describe('TaskService', () => {
  let taskService: TaskService;
  let httpMock: HttpTestingController;
  //let baseUri = environment.apiBaseUri;
 // var taskEndpoint = `${this.baseUri}${environment.endpoint_task_get}`;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TaskService,HttpClient]
    });
  });

  beforeEach(() => {
    taskService = TestBed.get(TaskService);
    httpMock = TestBed.get(HttpTestingController);
  });
  it('should be created', () => {
    expect(taskService).toBeTruthy();
  });

  it('getTasklist: should return an Observable<Array<Task>> ',  () => {
   
    const dummyTask: Task[] = [{
      Task_ID: 2,
      Task: 'Unit testing task',
      Start_Date: '14-10-2019',
      End_Date: '15-10-2019',
      Priority: 10,
     // Parent: 'string'
    }];
    
    let dummyData: ApiResponse<Task[]> = {Success:true,Data: dummyTask};

    taskService.getTasksList().subscribe((data) => {
      console.log("data:"+data);
        expect(data.Data).toEqual(dummyTask);

      });
      
      const req = httpMock.expectOne(request => request.url === `http://172.18.3.174:3000/tasks`);
     // console.log("url:"+req.url)
      expect(req.request.method).toBe('GET');
      req.flush(dummyData);
      httpMock.verify();
    });
});
