import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreateComponent } from './create.component';
import { Task, ParentTask } from '../../models/task';
import { TaskService } from '../../services/task.service';
import { RouterTestingModule } from '@angular/router/testing';

import { AlertService } from '../../../shared/services/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClientTestingModule, HttpTestingController, } from '@angular/common/http/testing';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { ToastrService } from 'ngx-toastr';
import { ParentSearchComponent } from '../search/parent-search/parent-search.component';
import { DateCompareValidatorDirective } from 'src/app/modules/shared/directives/datecompare.directive';
xdescribe('CreateComponent', () => {
  let component: CreateComponent;
  let fixture: ComponentFixture<CreateComponent>;
  const altSvc = {'success':  (a,b,c,d) => {
    return 'Data got successfully';
  },'error': (a,b,c,d) => {
    return 'Error'
  }, 'warn': (a,b,c,d) => {
    return 'WARN'}};

//   const router = { navigate: jasmine.createSpy('navigate'), events: {
//     subscribe: (fn: (data) => void) => fn({
//       taskType: 'search',
//     }),
// } };

const activatedRoute = {
  data: {
    subscribe: (fn: (data) => void) => fn({
      task: 'task id 1',
    }),
},
params: {  subscribe: (fn: (data) => void) => fn({
  taskId: 'test',
})},
  snapshot: {

    paramMap: {
      get: () => {
        return 'Task 1';
      }
    }
  },

};
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateComponent, ParentSearchComponent,DateCompareValidatorDirective ],
      imports: [
        RouterTestingModule,
        FormsModule,
        HttpClientTestingModule,
        NgbModule,
        ToastrModule.forRoot()
      ],
      providers: [
        TaskService, 
        {provide: AlertService, usevalue: altSvc},
        ToastrService,
        // { provide: Router, useValue: router },
        { provide: ActivatedRoute, useValue: activatedRoute }
       // TaskService
      ],
    }).overrideComponent(
      ParentSearchComponent, { set: { selector: 'parent-task-search', template: '<h2>container Component</h2>' } })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
