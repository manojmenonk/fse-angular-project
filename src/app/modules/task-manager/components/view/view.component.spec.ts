import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { Task } from '../../models/task';
import { TaskService } from '../../services/task.service';
import { RouterTestingModule } from '@angular/router/testing';

import { AlertService } from '../../../shared/services/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ViewComponent } from './view.component';
import { HttpClientTestingModule, HttpTestingController, } from '@angular/common/http/testing';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { ToastrService } from 'ngx-toastr';
describe('ViewComponent', () => {
  let component: ViewComponent;
  let fixture: ComponentFixture<ViewComponent>;
  let backend: HttpTestingController;
  const altSvc = {'success':  (a,b,c,d) => {
    return 'Data got successfully';
  },'error': (a,b,c,d) => {
    return 'Error'
  }, 'warn': (a,b,c,d) => {
    return 'WARN'}};

  const router = { navigate: jasmine.createSpy('navigate'), events: {
    subscribe: (fn: (data) => void) => fn({
      taskType: 'search',
    }),
} };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewComponent ],
      imports: [
        RouterTestingModule,
        FormsModule,
        HttpClientTestingModule,
        NgbModule,
        ToastrModule.forRoot()
      ],
      providers: [
        TaskService, 
        {provide: AlertService, usevalue: altSvc},
        ToastrService,
        { provide: Router, useValue: router }
       // TaskService
      ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewComponent);
    component = fixture.debugElement.componentInstance;
    backend = TestBed.get(HttpTestingController);
    
    
    const task1 : Task[] = [{
      Task_ID: 2,
      Task: 'sample first task',
      Start_Date: null,
      End_Date: null,
      Priority: 10
    }];
    component.Tasks = task1;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should render card title as  'Sort task label'`, () => {

    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('label').textContent).toContain('Sort Task By:');
  });
  it(`should render card title as  'button'`, () => {

    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('button').textContent).toContain('Start Date');
  });
 
});
