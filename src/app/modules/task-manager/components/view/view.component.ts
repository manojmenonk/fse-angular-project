import { Component, OnInit } from '@angular/core';
import { Task } from '../../models/task';
import { TaskService } from '../../services/task.service';
import { AlertService } from '../../../shared/services/alert.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  //project: Project;
  Tasks: Task[];
  SortKey: string;
  SearchKey: string;
  priorityFrom: string;
  priorityTo: string;
  startDate: string;
  endDate: string;
  constructor(private taskService: TaskService, private alertService: AlertService,
    private router: Router) { }

  ngOnInit() {
    this.sortTask('Start_Date');
}

  editTask(taskId: number) {
    this.taskService.getTask(taskId)
      .subscribe(response => {
        if (response.Success == true) {
          this.router.navigate(['/task/add'], { queryParams: { taskId: taskId } });
        }
        else {
          this.alertService.error(response.Message, 'Error', 3000);
        }
      });
  }

  endTask(taskId: number) {
    this.taskService.endTask(taskId)
      .subscribe(response => {
        if (response.Success == true) {
          this.refreshList();
          this.alertService.success('Task ended successfully!', 'Success', 3000);
        }
        else {
          this.alertService.error(response.Message, 'Error', 3000);
        }
      });
  }

  sortTask(sortKey: string) {
    
    this.SortKey = sortKey;
    this.taskService.getTasksList(sortKey)
      .subscribe(response => {
        if (response.Success == true) {
          this.Tasks = response.Data;
        }
        else {
          this.alertService.error(response.Message, 'Error', 3000);
        }
      });
  }
  searchTaskByName(searchKey: string) {
    console.log("view serachKey-->"+searchKey);
    
    this.taskService.getByTasksName(searchKey,"Task")
    .subscribe(response => {
      if (response.Success == true) {
        this.Tasks = response.Data;
      }
      else {
        this.alertService.error(response.Message, 'Error', 3000);
      }
    });
  }

  searchTaskByParentTask(searchKey: string) {
    console.log("view serachKey-->"+searchKey);
    
    this.taskService.searchByParentTask(searchKey,"ParentTask")
    .subscribe(response => {
      if (response.Success == true) {

        console.log("respose data"+response.Data);
        this.Tasks = response.Data;
      }
      else {
        this.alertService.error(response.Message, 'Error', 3000);
      }
    });
  }

  searchItems(){
    //console.log("input field value : "+ this.priorityFrom);
    //console.log("input field value : "+ this.priorityTo);
    if(this.priorityFrom != null && this.priorityTo != null){
      this.SearchKey = this.priorityFrom+":"+this.priorityTo;
      this.taskService.getByTasksName(this.SearchKey,"Priority")
      .subscribe(response => {
        if (response.Success == true) {
          this.Tasks = response.Data;
        }
        else {
          this.alertService.error(response.Message, 'Error', 3000);
        }
      });
    }else if(this.startDate != null){
      
      this.SearchKey =  moment(this.startDate).add(-1, 'months').format("YYYY-MM-DD");
      console.log("input field value : "+ this.SearchKey);
      this.taskService.getByTasksName(this.SearchKey,"StartDate")
      .subscribe(response => {
        if (response.Success == true) {
          this.Tasks = response.Data;
        }
        else {
          this.alertService.error(response.Message, 'Error', 3000);
        }
      });
  }
  else if(this.endDate != null){
    this.SearchKey = moment(this.endDate).add(-1, 'months').format("YYYY-MM-DD");
    this.taskService.getByTasksName(this.SearchKey,"EndDate")
    .subscribe(response => {
      if (response.Success == true) {
        this.Tasks = response.Data;
      }
      else {
        this.alertService.error(response.Message, 'Error', 3000);
      }
    });
}
  }

  refreshList() {
    //fetch all tasks associated to this project and display
    this.taskService.getTasksList(this.SortKey)
      .subscribe(response => {
        if (response.Success == true) {
          if (response.Data.length == 0) {
            this.alertService.warn('No taks found', 'Warning', 3000);
          }

          this.Tasks = response.Data;
          console.log(this.Tasks);
        }
        else {
          this.alertService.error('Error occured while fetching tasks ', 'Error', 3000);
        }
      });
  }

  

  //callback from Project search popup
  // onProjectSelected(project: Project) {
  //   this.project = project;
  //   this.refreshList();
  // }

}


